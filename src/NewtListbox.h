#pragma once

#include <string>
using namespace std;

#include "NewtComponent.h"

class NewtListbox : public NewtComponent {
public:
    NewtListbox(int height, int top = -1, int left = 0);

    void addEntry(const char* label, void* data);
    void addEntry(string label, void* data);
    void* getSelected();
    long getSelectedAsLong();
};
