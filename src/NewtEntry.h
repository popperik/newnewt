#pragma once

#include <string>
using namespace std;

#include "NewtComponent.h"

class NewtEntry : public NewtComponent {
public:
    NewtEntry(int width, int top = -1, int left = 0);

    string getText();
    void setText(const char* text);
    void setText(string text);
};
