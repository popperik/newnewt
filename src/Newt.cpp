#include "Newt.h"

Newt::Newt() {
    newtInit();
}

Newt::~Newt() {
    newtFinished();
}

void Newt::pushHelpLine(const char* text) {
    newtPushHelpLine(text);
    newtRefresh();
}

void Newt::pushHelpLine(string text) {
    Newt::pushHelpLine(text.c_str());
}

NewtWindow Newt::window(int width, int height, const char* title) {
    return NewtWindow(width, height, title);
}

NewtWindow Newt::window(int width, int height, string title) {
    return window(width, height, title.c_str());
}
