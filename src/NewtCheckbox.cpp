#include "NewtCheckbox.h"

NewtCheckbox::NewtCheckbox(const char* label, int top, int left)
    : NewtComponent(newtCheckbox(left, top, label, ' ', " X", nullptr)){}

NewtCheckbox::NewtCheckbox(string label, int top, int left)
    : NewtCheckbox(label.c_str(), top, left) {}

bool NewtCheckbox::isChecked() {
    return newtCheckboxGetValue(component) == 'X';
}

void NewtCheckbox::setChecked(bool checked) {
    newtCheckboxSetValue(component, checked ? 'X' : ' ');
}
