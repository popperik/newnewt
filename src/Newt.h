#pragma once

#include <string>
using namespace std;

#include "NewtWindow.h"

class Newt {
public:
    Newt();
    ~Newt();

    void pushHelpLine(const char* text);
    void pushHelpLine(string text);
    NewtWindow window(int width, int height, const char* title);
    NewtWindow window(int width, int height, string title);
};
