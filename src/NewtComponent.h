#pragma once

#include <newt.h>

class NewtComponent {
protected:
    newtComponent component;
    NewtComponent(newtComponent component);
public:
    newtComponent get() const;
};
