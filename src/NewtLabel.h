#pragma once

#include <string>
using namespace std;

#include "NewtComponent.h"

class NewtLabel : public NewtComponent {
public:
    NewtLabel(const char* text, int top = -1, int left = 0);
    NewtLabel(string text, int top = -1, int left = 0);
};
