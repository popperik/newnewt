#include "NewtEntry.h"

NewtEntry::NewtEntry(int width, int top, int left)
    : NewtComponent(newtEntry(left, top, "", width, nullptr, 0)) {}

string NewtEntry::getText() {
    return string(newtEntryGetValue(component));
}

void NewtEntry::setText(const char* text) {
    newtEntrySet(component, text, 1);
}

void NewtEntry::setText(string text) {
    setText(text.c_str());
}
