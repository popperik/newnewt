#include "NewtComponent.h"

NewtComponent::NewtComponent(newtComponent component)
    : component(component) {}

newtComponent NewtComponent::get() const {
    return component;
}
