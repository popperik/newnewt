#include "NewtButton.h"

NewtButton::NewtButton(const char* label, int top, int left)
    : NewtComponent(newtCompactButton(left, top, label)){}

NewtButton::NewtButton(string label, int top, int left)
    : NewtButton(label.c_str(), top, left){}
