#include "NewtWindow.h"

NewtWindow::NewtWindow(int width, int height, const char* title) {
    newtCenteredWindow(width, height, title);
}

NewtWindow::NewtWindow(int width, int height, string title)
    : NewtWindow(width, height, title.c_str()) {}

NewtWindow::~NewtWindow() {
    if (form) {
        newtFormDestroy(form);
        newtPopWindow();
    }
}

NewtWindow::NewtWindow(NewtWindow&& other)
    : form(other.form), nextLine(other.nextLine) {
    other.form = nullptr;
}

NewtWindow& NewtWindow::operator=(NewtWindow&& other) {
    if (form) {
        newtFormDestroy(form);
        newtPopWindow();
    }

    form = other.form;
    nextLine = other.nextLine;

    other.form = nullptr;
    return *this;
}

void NewtWindow::addComponent(NewtComponent component) {
    addComponent(component.get());
}

void NewtWindow::addComponent(newtComponent component) {
    newtFormAddComponent(form, component);
    int width, height;
    newtComponentGetSize(component, &width, &height);
    nextLine += height;
}

NewtButton NewtWindow::addButton(const char* label, int top, int left) {
    if (top < 0) {
        top = nextLine;
    }
    NewtButton button(label, top, left);
    addComponent(button);
    return button;
}

NewtButton NewtWindow::addButton(string label, int top, int left) {
    return addButton(label.c_str(), top, left);
}

NewtCheckbox NewtWindow::addCheckbox(const char* label, int top, int left) {
    if (top < 0) {
        top = nextLine;
    }

    NewtCheckbox checkbox(label, top, left);
    addComponent(checkbox);
    return checkbox;
}

NewtCheckbox NewtWindow::addCheckbox(string label, int top, int left) {
    return addCheckbox(label.c_str(), top, left);
}

NewtEntry NewtWindow::addEntry(int width, int top, int left) {
    if (top < 0) {
        top = nextLine;
    }
    NewtEntry entry(width, top, left);
    addComponent(entry);
    return entry;
}

NewtLabel NewtWindow::addLabel(const char* text, int top, int left) {
    if (top < 0) {
        top = nextLine;
    }
    NewtLabel label(text, top, left);
    addComponent(label);
    return label;
}

NewtLabel NewtWindow::addLabel(string text, int top, int left) {
    return addLabel(text.c_str(), top, left);
}

NewtListbox NewtWindow::addListbox(int height, int top, int left) {
    if (top < 0) {
        top = nextLine;
    }

    NewtListbox listbox(height, top, left);
    addComponent(listbox);
    return listbox;
}

void NewtWindow::addSpace(int rows) {
    nextLine += rows;
}

NewtExitCause NewtWindow::go() {
    newtExitStruct es;
    newtFormRun(form, &es);
    return es;
}
