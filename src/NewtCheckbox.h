#pragma once

#include <string>
using namespace std;

#include "NewtComponent.h"

class NewtCheckbox : public NewtComponent {
public:
    NewtCheckbox(const char* label = nullptr, int top = -1, int left = 0);
    NewtCheckbox(string label = "", int top = -1, int left = 0);

    bool isChecked();
    void setChecked(bool checked);
};
