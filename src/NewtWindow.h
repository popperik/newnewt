#pragma once

#include <newt.h>
#include <string>
using namespace std;

#include "NewtButton.h"
#include "NewtCheckbox.h"
#include "NewtEntry.h"
#include "NewtExitCause.h"
#include "NewtLabel.h"
#include "NewtListbox.h"

class NewtWindow {
    newtComponent form = newtForm(0, 0, 0); // TODO Change to NewtForm?
    int nextLine = 0;

    void addComponent(newtComponent component);
public:
    NewtWindow(int width, int height, const char* title);
    NewtWindow(int width, int height, string title);
    ~NewtWindow();

    NewtWindow(const NewtWindow& other) = delete;
    void operator=(const NewtWindow& other) = delete;
    NewtWindow(NewtWindow&& other);
    NewtWindow& operator=(NewtWindow&& other);

    void addComponent(NewtComponent component);
    NewtButton addButton(const char* label, int top = -1, int left = 0);
    NewtButton addButton(string label, int top = -1, int left = 0);
    NewtCheckbox addCheckbox(const char* label = nullptr, int top = -1, int left = 0);
    NewtCheckbox addCheckbox(string label = "", int top = -1, int left = 0);
    NewtEntry addEntry(int width, int top = -1, int left = 0);
    NewtLabel addLabel(const char* text, int top = -1, int left = 0);
    NewtLabel addLabel(string text, int top = -1, int left = 0);
    NewtListbox addListbox(int height, int top = -1, int left = 0);
    void addSpace(int rows = 1);
    NewtExitCause go();
};
