#pragma once

enum class NewtKey {
    TAB        = '\t',
    ENTER      = '\r',
    SUSPEND    = '\032', // ctrl - z
    ESCAPE     = '',
    RETURN     = ENTER,

    EXTRA_BASE = 0x8000,
    UP         = EXTRA_BASE + 1,
    DOWN       = EXTRA_BASE + 2,
    LEFT       = EXTRA_BASE + 4,
    RIGHT      = EXTRA_BASE + 5,
    BKSPC      = EXTRA_BASE + 6,
    DELETE     = EXTRA_BASE + 7,
    HOME       = EXTRA_BASE + 8,
    END        = EXTRA_BASE + 9,
    UNTAB      = EXTRA_BASE + 10,
    PGUP       = EXTRA_BASE + 11,
    PGDN       = EXTRA_BASE + 12,
    INSERT     = EXTRA_BASE + 13,

    F1         = EXTRA_BASE + 101,
    F2         = EXTRA_BASE + 102,
    F3         = EXTRA_BASE + 103,
    F4         = EXTRA_BASE + 104,
    F5         = EXTRA_BASE + 105,
    F6         = EXTRA_BASE + 106,
    F7         = EXTRA_BASE + 107,
    F8         = EXTRA_BASE + 108,
    F9         = EXTRA_BASE + 109,
    F10        = EXTRA_BASE + 110,
    F11        = EXTRA_BASE + 111,
    F12        = EXTRA_BASE + 112,

/* not really a key, but newtGetKey returns it */
    RESIZE     = EXTRA_BASE + 113,
    ERROR      = EXTRA_BASE + 114
};
