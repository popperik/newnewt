#include "NewtLabel.h"

NewtLabel::NewtLabel(const char* text, int top, int left)
    : NewtComponent(newtLabel(left, top, text)) {}

NewtLabel::NewtLabel(string text, int top, int left)
    : NewtLabel(text.c_str(), top, left) {}
