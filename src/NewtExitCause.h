#pragma once

#include "NewtComponent.h"
#include "NewtKey.h"

class NewtExitCause {
    newtExitStruct es;
public:
    NewtExitCause(newtExitStruct es);

    bool operator==(const NewtComponent& component);
    bool operator!=(const NewtComponent& component);
    bool operator==(const NewtKey& key);
    bool operator!=(const NewtKey& key);
};
