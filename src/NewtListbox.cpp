#include "NewtListbox.h"

NewtListbox::NewtListbox(int height, int top, int left)
    : NewtComponent(newtListbox(left, top, height, NEWT_FLAG_RETURNEXIT)) {}

void NewtListbox::addEntry(const char* label, void* data) {
    newtListboxAppendEntry(component, label, data);
}

void NewtListbox::addEntry(string label, void* data) {
    addEntry(label.c_str(), data);
}

void* NewtListbox::getSelected() {
    return newtListboxGetCurrent(component);
}

long NewtListbox::getSelectedAsLong() {
    return (long) getSelected();
}
