#pragma once

#include <string>
using namespace std;

#include "NewtComponent.h"

class NewtButton : public NewtComponent {
public:
    NewtButton(const char* label, int top = -1, int left = 0);
    NewtButton(string label, int top = -1, int left = 0);
};
