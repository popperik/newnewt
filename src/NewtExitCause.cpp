#include "NewtExitCause.h"

NewtExitCause::NewtExitCause(newtExitStruct es)
    : es(es) {}

bool NewtExitCause::operator==(const NewtComponent& component) {
    return es.reason == newtExitStruct::NEWT_EXIT_COMPONENT && es.u.co == component.get();
}

bool NewtExitCause::operator!=(const NewtComponent& component) {
    return !operator==(component);
}

bool NewtExitCause::operator==(const NewtKey& key) {
    return es.reason == newtExitStruct::NEWT_EXIT_HOTKEY && es.u.key == (int) key;
}

bool NewtExitCause::operator!=(const NewtKey& key) {
    return !operator==(key);
}
